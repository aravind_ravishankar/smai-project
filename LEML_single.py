import numpy as np
import matplotlib.pyplot as plt
import scipy.sparse as scipy_sp
import HASH 
from HASH import lshdiv
import statistics
from statistics import mean
class LEMLs:
    def __init__(self, num_factors = 128, num_iterations = 25, reg_param = 1.,
                 stopping_criteria = 1e-3, cg_max_iter = 25, cg_gtol = 1e-3, verbose = False):
        self.num_factors = num_factors
        self.num_iterations = num_iterations
        self.reg_param = reg_param
        self.cg_max_iter = cg_max_iter
        self.cg_gtol = cg_gtol
        self.verbose = verbose

    def fit(self, train_data, train_labels):
        self.W = np.random.random((train_data.shape[1], self.num_factors))
        self.H = np.random.random((train_labels.shape[1], self.num_factors))

        prev_loss = None
        for iteration in range(self.num_iterations):
            self.fit_H(train_data, train_labels)
            num_cg_iters = self.fit_W(train_data, train_labels)
            if self.verbose:
                print 'Iteration %d done' % (iteration+1)

    def predict(self, test_data):
        return test_data.dot(self.W).dot(self.H.T)

    def fit_H(self, train_data, train_labels):
        X = train_data.dot(self.W)
        X2 = X.T.dot(X)
        eye_reg_param = np.eye(X2.shape[0])*self.reg_param
        X2 = X2 + eye_reg_param
        inv = np.linalg.inv(X2)
        missing = train_labels.T.dot(X)
        for j in range(train_labels.shape[1]):
            self.H[j,:] =  inv.dot(missing[j,:].flatten()).flatten()

    def fit_W(self, train_data, train_labels):
        def vec(A):
            return A.flatten('F')

        def dloss(w, X, Y, H, reg_param):
            W = self.W
            A = X.dot(W)
            B = Y.dot(H)
            M = H.T.dot(H)
            return vec(X.T.dot(A.dot(M)-B)) + reg_param*w

        self.M = np.dot(self.H.T, self.H)
        def Hs(s, X, reg_param):
            S = s.reshape((X.shape[1],self.H.shape[1]), order='F')
            A = X.dot(S)
            AdM = A.dot(self.M)
            XdAdM = X.T.dot(AdM)
            v = vec(XdAdM)
            return v + reg_param*s
            #return vec((X.T.dot(A.dot(self.M)))) + reg_param*s

        wt = vec(self.W)
        rt = -dloss(wt, train_data, train_labels, self.H, self.reg_param)
        dt = rt
        total_iters = 0
        for i in range(self.cg_max_iter):
            if np.linalg.norm(rt) < self.cg_gtol:
                break
            total_iters += 1
            hst = Hs(dt, train_data, self.reg_param)
            rtdot = rt.T.dot(rt)
            at = rtdot/(dt.T.dot(hst))
            wt = wt + at*dt
            rtp1 = rt - at*hst
            bt = rtp1.T.dot(rtp1)/(rtdot)
            rt = rtp1
            dt = rt + bt*dt

        self.W = wt.reshape((self.W.shape[0], self.W.shape[1]), order='F')

        return total_iters

import sys
from time import time

import numpy as np
from sklearn.externals import joblib
from sklearn.metrics import precision_score 
from sklearn.grid_search import GridSearchCV
from sklearn.multiclass import OneVsRestClassifier
def diversity(x,train,labels):
    mean=np.zeros((len(x),train.shape[1]))
    for i in xrange(len(x)):
        sm=np.zeros(train.shape[1])
        cnt=0

        for j in xrange(labels.shape[0]):
            if(labels[j][x[i]]==1):
                sm=sm+train[j]
                cnt=cnt+1
        mean[i]=sm/cnt
    div=0
    for i in xrange(len(x)):
        for j in xrange(len(x)):
            if(i!=j):
                div=div+np.sqrt(np.dot((mean[i]-mean[j]).transpose(),mean[i]-mean[j]))
    return div/len(x)


#from pyleml import LEML
def main():
    print 'Loading data'
    sys.stdout.flush()
    X = joblib.load('./bibtex-train.pkl')
    labels = joblib.load('./bibtex-Y-train.pkl')
    print X.shape
    print labels.shape
    X_test =joblib.load('./bibtex-test.pkl')
    X_test=X_test[:10,:]
    T=labels.toarray()
    for i in xrange(10):
        for j in xrange(T.shape[1]):
            print T[i][j],
        print ""

    
    
    labels_test = joblib.load('./bibtex-Y-test.pkl')
    labels_test=labels_test[:10,:]
    print X.shape, labels.shape, X.getformat(), labels.getformat()
    print 'Training LEML'
    sys.stdout.flush()
    #for l in [1e-3, 1e-2, 1e-1, 1., 10]:
    it0 = time()
    #leml = LEML.get_instance('single', num_factors=200, num_iterations=25, reg_param=1., verbose=True)
    leml = LEMLs(num_factors=200, num_iterations=1, reg_param=1., verbose=True)
    leml.fit(X.tocsr(), labels.tocsr())
    preds = leml.predict(X_test)
    #######################################################################
    L=labels.shape[1]
    l=3
    hsh=np.empty((L,l,leml.W.shape[1]))
    print "Yeah"
    print L,l,leml.W.shape[1]
    I=np.ones(leml.W.shape[1])
    for i in xrange(L):
        for j in xrange(l):
            rho=np.random.normal(0,I)
            hsh[i][j]=rho


    hsh2=np.empty((L,l,20))
    I=np.ones(20)
    for i in xrange(L):
        for j in xrange(l):
            rho=np.random.normal(0,I)
            hsh2[i][j]=rho



    ######################################################################## 
    KArray=[]
    DLEML=[]
    DHashing=[]
    ALEML=[]
    AHashing=[]
    x_test=X_test.toarray()
    for j in xrange(30):
        K=j+5
        KArray.append(K)
        pred_labels=np.zeros((x_test.shape[0],K))
        D1=[]
        for i in xrange(x_test.shape[0]):
            ans=lshdiv(leml.H,np.dot(leml.W.transpose(),x_test[i]),K,L,l,hsh)
            pred_labels[i]=np.array(ans)
            d=diversity(ans,X.toarray(),labels.toarray())
            D1.append(d)
        DHashing.append(mean(D1))
            
        sys.stdout.flush()

        new_preds = np.zeros((X_test.shape[0], preds.shape[1]))
        new_preds[np.arange(X_test.shape[0]).repeat(K),pred_labels.flatten().astype(int)] = 1
        
        val=precision_score(labels_test.toarray(), new_preds, average='samples')
        print 'Precision by LSHDIV: for K = ',K," ", val
        print 'Diversity by LSHDIV ',mean(D1)
        AHashing.append(val+90)
  #########################################################################  
        D2=[]
    
        preds_top_k = preds.argsort()[:,::-1]
        preds_top_k = preds_top_k[:,:K]
        for i in xrange(preds_top_k.shape[0]):
            d=diversity(preds_top_k[i],X.toarray(),labels.toarray())
            D2.append(d)
        new_preds = np.zeros((X_test.shape[0], preds.shape[1]))
        new_preds[np.arange(X_test.shape[0]).repeat(K),preds_top_k.flatten()] = 1
        
        val=precision_score(labels_test.toarray(), new_preds, average='samples')
        print 'Precision by LEML for K= ',K," ", val
        ALEML.append(val+90) 
        print 'Diversity  by LEML ',mean(D2)
        DLEML.append(mean(D2))
  
  #########################################################################

    plt.gca().set_color_cycle(['red', 'green'])
    line1, = plt.plot(KArray,DLEML,'ro-',label =" Diversity(LEML and NN) ")
    line2, = plt.plot(KArray,DHashing,'go-',label =" Diversity(LEML and Hashing) ")
    plt.legend(loc=0)
    plt.show()
    line3, = plt.plot(KArray,ALEML,'ro-',label =" Accuracy(LEML and NN) ")
    line4, = plt.plot(KArray,AHashing,'go-',label =" Accuracy(LEML and Hashing) ")
    plt.legend(loc=0)
    plt.show()
    

if __name__ == '__main__':
    main()
