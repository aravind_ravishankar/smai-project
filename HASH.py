import numpy as np
from sklearn.decomposition import PCA
def gethash(x,y):
        return np.sign(np.dot(x,y))
def lshdiv(X,q,K,L,l,hsh):
    table=np.empty((X.shape[0],L,l))
    R=[]
    for i in xrange(X.shape[0]):
        for j in xrange(L):
            for k in xrange(l):
                temp=hsh[j][k]
                table[i][j][k]=gethash(X[i],temp)
    for i in xrange(L):
        hashq=np.zeros(l)
        for j in xrange(l):
            temp=hsh[i][j]
            hashq[j]=gethash(q,temp)
        for k in xrange(X.shape[0]):
            collision=True
            for m in xrange(l):
                if(table[k][i][m]!=hashq[j]):
                    collision=False
                    break
            if(collision==True):
                if k not in R:
                    R.append(k)
    if(len(R) < K):
        return R
    S=[]
    lamda=0.7
    for i in xrange(K):
        index=0
        mn=float("inf")
        for j in xrange(len(R)):
            sm=0
            for m in xrange(len(S)):
                t1=X[R[j]]-X[S[m]]
                sm=sm+np.dot(t1,t1)
            t2=(X[R[j]]-q)
            if(mn>lamda*np.dot(t2,t2)-(1-lamda)*sm):
                index=j
                mn=lamda*np.dot(t2,t2)-(1-lamda)*sm
        S.append(R[index])
        del R[index]
    return S






                

        






                
                
                

                

                    

                







