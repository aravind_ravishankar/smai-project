import numpy as np
import os
from numpy  import genfromtxt
import matplotlib.pyplot as plt
import scipy.sparse as scipy_sp
import HASH 
from HASH import lshdiv
import statistics
from statistics import mean
from sklearn.externals import joblib
from sklearn.metrics import precision_score 
from sklearn.grid_search import GridSearchCV
from sklearn.multiclass import OneVsRestClassifier
from sklearn.decomposition import PCA
C=[]
SC=[]
NumberOfLabels=32
NumberOfSubCategories=4
NumberOfCategories=6

ANS=[]
count=[]
def create():
    global count
    global ANS
    count=[0]*6
    for i in range(6):
        SC.append([])
        ANS.append([])
    count[0]=20
    SC[0].append("jasmine")
    SC[0].append("lily")
    SC[0].append("rose")
    SC[0].append("tulip")
    SC[0].append("sunflower")
    ANS[0]=[0]*5
    count[1]=20
    SC[1].append("bike")
    SC[1].append("cycle")
    SC[1].append("jeep")
    SC[1].append("car")
   
    ANS[1]=[0]*4
    count[2]=19
    SC[2].append("flask")
    SC[2].append("jug")
    SC[2].append("wine")
    SC[2].append("ink")

    ANS[2]=[0]*4
   
    count[3]=22
    SC[3].append("aquatic")
    SC[3].append("birds")
    SC[3].append("range")
    SC[3].append("predator")

    ANS[3]=[0]*4

    count[4]=20
    SC[4].append("bed")
    SC[4].append("chair")
    SC[4].append("cupboard")
    SC[4].append("table")

    ANS[4]=[0]*4

    count[5]=20
    SC[5].append("accordian")
    SC[5].append("allinstru")
    SC[5].append("piano")
    SC[5].append("table")
    SC[5].append("violin")

    ANS[5]=[0]*5
    
def diversity(S,N):
    for i in S:
        for j in xrange(len(SC)):
            for k in xrange(len(SC[j])):
                if SC[j][k] in N[i]:
                    
                    ANS[j][k]+=1
                    print "yes"
    Diversity=0
    for i in xrange(len(count)):
        D1=0
        print "here"
        for j in xrange(len(ANS[i])):
            if (ANS[i][j] !=0):
                D1=D1 + (ANS[i][j]/5.0)*np.log(ANS[i][j]/5.0)
                ANS[i][j]=0
        Diversity=Diversity+D1*1.0/count[i]
    return Diversity
def accuracy(S,q):
    Accuracy=0
    for i in xrange(len(S)):
        Accuracy=Accuracy+ np.dot((S[i]-q[i]).transpose(),(S[i]-q[i]))
    return 100/Accuracy
def hscore(querypoints,Diversity,Accuracy):
    ans=0
    for  i in range(querypoints):
        ans=ans+2*Diversity[i]*Accuracy[i]/(Diversity[i]+Accuracy[i])
    return ans

def NN(X,q,K):
    nn=[]
    for i in xrange(X.shape[0]):
        dist=np.sqrt(np.dot((X[i]-q).transpose(),X[i]-q))
        nn.append(tuple([dist,i]))

    nn.sort()
    ans=[]
    for  i in xrange(K):
        ans.append(nn[i][1])
    return ans
    


def main():
    print 'Loading data'
    with open("a.txt") as f:
        N=f.readlines()
    NumberOfLabels=32
    querypoints=[]
    my_data = genfromtxt('a.csv', delimiter=',')



    print "\nOptions to Choose from 1.Nearest Neighbour Search\n2.LSHDIV\n3.LSHSDIV"
    option=input("Enter Option you want Image Outputs For") 
    Nq=input("NumberOfQueryPoints needed")
    DPATH=raw_input( "Enter Directory Path to read images from")
    print "Enter Indices of query points needed"

    for i in range(Nq):
        r=input("Enter Index : ")
        querypoints.append(my_data[r])
    
    create()
    
    L=NumberOfLabels
    l=3
   
   ################################################################################################
   #HASH FUNCTION FOR LSHDIV
     
    hsh=np.empty((L,l,my_data.shape[1]))
    I=np.ones(my_data.shape[1])
    for i in xrange(L):
        for j in xrange(l):
            rho=np.random.normal(0,I)
            hsh[i][j]=rho

    
    ##################################################################################################
    #HASH FUNCTION FOR LSHSDIV
    alpha=3

    hsh2=np.empty((L,l,alpha))
    I=np.ones(alpha)
    for i in xrange(L):
        for j in xrange(l):
            rho=np.random.normal(0,I)
            hsh2[i][j]=rho

    K=3
    #####################################################################################################
    LSHShscore=0
    LSHhscore=0
    XNN=[]
    XLSHS=[]
    XLSH=[]
    XNN=[]
    XLSHS=[]
    ANN=[]
    ALSH=[]
    ALSHS=[]
    DNN=[]
    DLSH=[]
    DLSHS=[]
    hscoreNN=[]
    hscoreLSHS=[]
    hscoreLSH=[]


    i=0
    for q in querypoints:

        XNN.append(i)
        XLSH.append(i)
        XLSHS.append(i)
        i=i+1

        #LSHSDIV
        #################################################################
        new=np.empty((my_data.shape[0]+1,my_data.shape[1]))
        new[0:my_data.shape[0],0:my_data.shape[1]]=my_data[0:,0:]
        pca=PCA(n_components=alpha)
        x=pca.fit_transform(new)
        S=lshdiv(x[0:my_data.shape[0]],x[my_data.shape[0]],K,L,l,hsh2)
        D=diversity(S,N)
        A=accuracy(S,q)
        hscore=2*D*A/(D+A)
        DLSHS.append(D)
        hscoreLSHS.append(hscore)
        ALSHS.append(A)
        
        if (option==3):
            for index in S:
                print index
                command="gnome-open " +  DPATH +"/" N[index]
                os.system(command)


    
    ####################################################################################################
        #LSHDIV
        #################################################################
        
        S=lshdiv(my_data,q,K,L,l,hsh)
        D=diversity(S,N)
        A=accuracy(S,q)
        hscore=2*D*A/(D+A)
        hscoreLSH.append(hscore)
        DLSH.append(D)
        ALSH.append(A)
        if(option==2):
            for index in S:
                command="gnome-open " +  DPATH +"/" N[index]
                os.system(command)




    ###################################################################################################
        #NN
        ################################################################
        
        
        S=NN(my_data,q,K)
        D=diversity(S,N)
        A=accuracy(S,q)

        hscore=2*D*A/(D+A)
        DNN.append(D)
        hscoreNN.append(hscore)
        ANN.append(A)
        
        if(option==1): 
            for index in S:
                command="gnome-open " +  DPATH +"/" N[index]
                os.system(command)




    line1, = plt.plot(XNN,hscoreNN,'ro-',label =" Hscore( NN) ")
    line2, = plt.plot(XLSH,hscoreLSH,'go-',label =" Hscore(LSHDIV) ")
    line3, = plt.plot(XLSHS,hscoreLSHS,'bo-',label =" Hscore(LSHSDIV) ")

    line4, = plt.plot(XNN,DNN,'ro-',label =" Diversity( NN) ")
    line5, = plt.plot(XLSH,DLSH,'go-',label =" Diversity( LSHDIV) ")
    line6, = plt.plot(XLSHS,DLSHS,'bo-',label =" Diversity(LSHSDIV) ")
    line7, = plt.plot(XLSHS,ALSHS,'ro-',label =" Accuracy(LSHSDIV) ")
    line8, = plt.plot(XLSH,ALSH,'go-',label =" Accuracye(LSHDIV) ")
    line9, = plt.plot(XLSHS,ANN,'bo-',label =" Accuracy( NN) ")

    plt.legend(loc=0)
    plt.show()
                                      

    ##################################################################################################
        

if __name__ == '__main__':
    main()
